# Project settings report

Create report of project attributes using the GitLab API and GitLab pages.

## Features

- Queries all projects in an instance, specific groups or projects
- Allows filtering on visibility
- Retrieves all project attributes from https://docs.gitlab.com/ee/api/projects.html#get-single-project
- Produces HTML CSV

## Usage

`python3 projects_report.py $GIT_TOKEN $CONFIG_YML`

## Configuration

- Export / fork repository.
- edit config.yml
  - specify GitLab URL and groups or projects using their ID.
  - If groups are used, all projects in the group are retrieved instead of individual projects.
  - If neither groups nor projects are specified, all projects from all top-level groups visible to the token will be retrieved.
  - Optionally configure `visibility` and `get_metadata` to filter projects and specify if ehanced metadata is needed (will take significantly longer to query)
  - a `read_api` token will be needed. If run with CI, it needs to be configued as CI variable `GIT_TOKEN`. It should be masked.
- Make sure Settings -> Permissions -> Pages is limited to project members
- Run the Pipeline to get your report
- access using pages

## Disclaimer

This script is provided for educational purposes. It is not supported by GitLab. However, you can create an issue if you need help or propose a Merge Request. This script only reads data via the GitLab API and does not perform write actions.
