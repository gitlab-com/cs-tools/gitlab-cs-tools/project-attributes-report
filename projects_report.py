#!/usr/bin/env python3

import gitlab
import csv
import argparse
import yaml
from mako.template import Template

def write_csv(project_objects, fields):
    file_name = "projects_report.csv"
    with open(file_name,"w") as report:
        reportwriter = csv.writer(report, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        reportwriter.writerow(fields)
        for project in project_objects:
            row = []
            for field in fields:
                if field in project:
                    row.append(project[field])
                else:
                    row.append("")
            reportwriter.writerow(row)

def get_project_settings(gl, projects):
    project_objects = []
    for project in projects:
        project_object = None
        if isinstance(project, str):
            project_object = gl.projects.get(project)
        else:
            project_object = gl.projects.get(project.id)
        # remove runner token to prevent leakage
        project_attributes = project_object.attributes
        if 'runners_token' in project_attributes:
            del project_attributes['runners_token']
        project_objects.append(project_attributes)
    return project_objects

def get_group_projects(gl, groups, visibility):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        if visibility:
            group_projects = group.projects.list(iterator=True, include_subgroups=True, visibility=visibility)
        else:
            group_projects = group.projects.list(iterator=True, include_subgroups=True)
        for group_project in group_projects:
            projects.append(group_project)
    return projects

def get_top_level_groups(gl):
    groups = []
    for group in gl.groups.list(iterator=True, top_level_only=True):
        groups.append(group.attributes["id"])
    return groups

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='CSV file that defines requested projects')
args = parser.parse_args()

gitlaburl = "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

configfile = args.configfile
groups = []
projects = []
fields = []
get_metadata = False
with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    gitlaburl = config["gitlab_url"] if config["gitlab_url"].endswith("/") else config["gitlab_url"] + "/"
    gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

    visibility = config.get("visibility")
    get_metadata = config.get("get_metadata")
    
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups, visibility)
    else:
        if not "projects" in config:
            # No group or project configured, use get_top_level_groups(gl), to get all top level groups. Then for each top level group, get_group_projects(gl, group)
            print("No group or project selected, getting everything accessible to the token")
            group_ids = get_top_level_groups(gl)
            projects = get_group_projects(gl, group_ids, visibility)
        else:
            projects = config["projects"]
            get_metadata = True

print("Found %i projects." % len(projects))

if get_metadata:
    print("Getting metadata")
    project_objects = get_project_settings(gl, projects)
else:
    project_objects = [project.attributes for project in projects]

if project_objects:
    fields = project_objects[0].keys()
    write_csv(project_objects, fields)
    mytemplate = Template(filename='template/index.html')

    with open("public/index.html","w") as outfile:
        #try:
        outfile.write(mytemplate.render(projects = project_objects, fields = fields))
        #except:
        #    print("Could not render HTML")
else:
    print("No projects found")

